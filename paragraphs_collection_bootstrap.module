<?php

/**
 * @file
 * Contains paragraphs_collection_bootstrap.module.
 */
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Implements hook_theme().
 */
function paragraphs_collection_bootstrap_theme($existing, $type, $theme, $path) {
  return [
    'pcb_accordion' => [
      'variables' => [
        'content' => [],
        'attributes' => [],
      ],
    ],
    'field__paragraph__pcb_progress' => [
      'base hook' => 'field',
      ],
    'pcb_carousel' => [
      'variables' => [
        'content' => [],
        'behavior' => [],
        'attributes' => [],
      ],
    ],
    'pcb_tabs' => [
      'variables' => [
        'content' => [],
        'settings' => [],
        'attributes' => [],
      ],
    ],
    'paragraph__pcb_card' => [
      'base hook' => 'paragraph',
    ],
    'paragraph__pcb_jumbotron' => [
      'base hook' => 'paragraph',
    ],
    'paragraph__pcb_button_group' => [
      'base hook' => 'paragraph',
    ],
    'field__field_pcb_button_group_container' => [
      'base hook' => 'field',
    ],
    'field__paragraph__pcb_button_group__field_pcb_button_link' => [
      'base hook' => 'field',
    ],
    'paragraph__pcb_button_group__paragraph__pcb_button' => [
      'base hook' => 'paragraph',
    ],
  ];
}

/**
 * Implements hook_preprocess_paragraph().
 */
function paragraphs_collection_bootstrap_preprocess_paragraph(&$variables) {
  if ($variables['paragraph']->bundle() == 'pcb_button') {
    $behavior_settings = $variables['paragraph']->getAllBehaviorSettings();
    $variables['#attached']['library'][] = 'bs_lib/bootstrap_css';

    if (isset($behavior_settings['style'])) {
      switch ($behavior_settings['style']['style']) {
        case 'button-primary':
          $variables['content']['field_pcb_button_link'][0]['#options']['attributes']['class'] = [
            'btn',
            'btn-primary',
          ];
          break;

        case 'button-secondary':
          $variables['content']['field_pcb_button_link'][0]['#options']['attributes']['class'] = [
            'btn',
            'btn-secondary',
          ];
          break;

        case 'button-success':
          $variables['content']['field_pcb_button_link'][0]['#options']['attributes']['class'] = [
            'btn',
            'btn-success',
          ];
          break;

        case 'button-info':
          $variables['content']['field_pcb_button_link'][0]['#options']['attributes']['class'] = [
            'btn',
            'btn-info',
          ];
          break;

        case 'button-warning':
          $variables['content']['field_pcb_button_link'][0]['#options']['attributes']['class'] = [
            'btn',
            'btn-warning',
          ];
          break;

        case 'button-danger':
          $variables['content']['field_pcb_button_link'][0]['#options']['attributes']['class'] = [
            'btn',
            'btn-danger',
          ];
          break;

        case 'button-link':
          $variables['content']['field_pcb_button_link'][0]['#options']['attributes']['class'] = [
            'btn',
            'btn-link',
          ];
          break;

        default:
          $variables['content']['field_pcb_button_link'][0]['#options']['attributes']['class'] = [
            'btn',
            'btn-secondary',
          ];
      }
    }
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function paragraphs_collection_bootstrap_theme_suggestions_field_alter(array &$suggestions, array $variables) {
  if ($variables['element']['#object'] instanceof Paragraph) {
    $object = $variables['element']['#object'];
    if ($object->toArray()['parent_field_name'][0]['value'] == 'field_pcb_button_group_container') {
      $suggestions[] = 'field__paragraph__pcb_button_group__field_pcb_button_link';
    }
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function paragraphs_collection_bootstrap_theme_suggestions_paragraph_alter(array &$suggestions, array $variables) {
  if ($variables['elements']['#paragraph'] instanceof Paragraph) {
    $object = $variables['elements']['#paragraph'];
    if ($object->toArray()['parent_field_name'][0]['value'] == 'field_pcb_button_group_container') {
      $suggestions[] = 'paragraph__pcb_button_group__paragraph__pcb_button';
    }
  }
}
