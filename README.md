#Paragraphs Collection

THIS PROJECT IS EXPERIMENTAL. DO NOT USE IT IN PRODUCTION PROJECTS. THERE IS NO
UPGRADE PATH UNTIL A BETA RELEASE.

CONTENT OF THIS FILE
--------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------
@todo


REQUIREMENTS
------------
This module requires the following modules:

 * Paragraphs (https://www.drupal.org/project/paragraphs)
 * Paragraphs Collection (https://www.drupal.org/project/paragraphs_collection)
 * BS Lib (https://www.drupal.org/project/bs_lib)

INSTALLATION
------------
Please check the module dependencies and eventually the README.md files of the
related modules to properly install everything you need.

CONFIGURATION
-------------
@todo

MAINTAINERS
-----------
Current maintainers:
 * @todo
