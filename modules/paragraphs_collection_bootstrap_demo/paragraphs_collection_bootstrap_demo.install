<?php

/**
 * @file
 * Installation hooks for paragraphs_collection_bootstrap_demo module.
 */

use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\ParagraphInterface;

/**
 * Implements hook_install().
 */
function paragraphs_collection_bootstrap_demo_install() {
  $node = _paragraphs_collection_bootstrap_demo_create_demo_article();
  \Drupal::configFactory()->getEditable('system.site')->set('page.front', '/node/' . $node->id())->save();
}

/**
 * Implements hook_uninstall().
 */
function paragraphs_collection_bootstrap_demo_uninstall() {
  $nids = \Drupal::entityQuery('node')->condition('type', 'bootstrap_paragraphed_content')->execute();
  if (!empty($nids)) {
    $nodes = Node::loadMultiple($nids);
    foreach ($nodes as $node) {
      /** @var \Drupal\node\Entity\Node $node */
      $node->delete();
    }
  }
}

/**
 * Create demo article example.
 *
 * @return \Drupal\Core\Entity\EntityInterface|static
 *   Returns node.
 */
function _paragraphs_collection_bootstrap_demo_create_demo_article() {
  $paragraphs = [];

  // Create Progress bar paragraphs.
  $default_progress_bar = _paragraphs_collection_bootstrap_demo_create_progress_bar_paragraph();
  $danger_progress_bar = _paragraphs_collection_bootstrap_demo_create_progress_bar_paragraph('bg-danger', TRUE, FALSE, FALSE, 15);
  $success_progress_bar = _paragraphs_collection_bootstrap_demo_create_progress_bar_paragraph('bg-success', TRUE, FALSE, TRUE, 10);
  $info_progress_bar = _paragraphs_collection_bootstrap_demo_create_progress_bar_paragraph('bg-info');
  $warning_progress_bar = _paragraphs_collection_bootstrap_demo_create_progress_bar_paragraph('bg-warning', TRUE, TRUE);
  $paragraphs[] = _paragraphs_collection_bootstrap_demo_create_progress_paragraph([$default_progress_bar]);
  $paragraphs[] = _paragraphs_collection_bootstrap_demo_create_progress_paragraph([$danger_progress_bar]);
  $paragraphs[] = _paragraphs_collection_bootstrap_demo_create_progress_paragraph([$info_progress_bar]);
  $paragraphs[] = _paragraphs_collection_bootstrap_demo_create_progress_paragraph([
    $success_progress_bar,
    $default_progress_bar,
    $warning_progress_bar,
  ]);

  // Create accordion with text paragraph.
  $content_paragraph_1 = _paragraphs_collection_bootstrap_demo_create_text_paragraphs('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');
  $accordion_item_1 = _paragraphs_collection_bootstrap_demo_create_paragraph_accordion_item('Accordion title #1', $content_paragraph_1);
  $content_paragraph_2 = _paragraphs_collection_bootstrap_demo_create_text_paragraphs('Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?');
  $accordion_item_2 = _paragraphs_collection_bootstrap_demo_create_paragraph_accordion_item('Accordion title #2', $content_paragraph_2);
  $content_paragraph_3 = _paragraphs_collection_bootstrap_demo_create_text_paragraphs('On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.');
  $accordion_item_3 = _paragraphs_collection_bootstrap_demo_create_paragraph_accordion_item('Accordion title #3', $content_paragraph_3);

  $paragraphs[] = _paragraphs_collection_bootstrap_demo_create_paragraph('pcb_accordion', 'field_pcb_accordion_container', [
    $accordion_item_1,
    $accordion_item_2,
    $accordion_item_3,
  ]);

  $carousel_item_1 = _paragraphs_collection_demo_create_paragraph_carousel_item('Red background', 'red-background-min.png', 'images/red-background-min.png');
  $carousel_item_2 = _paragraphs_collection_demo_create_paragraph_carousel_item('Blue background', 'blue-background-min.png', 'images/blue-background-min.png');
  $carousel_item_3 = _paragraphs_collection_demo_create_paragraph_carousel_item('Grey background', 'grey-background-min.png', 'images/grey-background-min.png');
  $carousel_item_4 = _paragraphs_collection_demo_create_paragraph_carousel_item('Brown background', 'brown-background-min.png', 'images/brown-background-min.png');

  $paragraphs[] = _paragraphs_collection_bootstrap_demo_create_paragraph('pcb_carousel', 'field_pcb_carousel_container', [
    $carousel_item_1,
    $carousel_item_2,
    $carousel_item_3,
    $carousel_item_4,
    ], [
    'controls' => TRUE,
    'indicator' => TRUE,
    'caption' => TRUE,
    ]);

  // Create tabs paragraphs.
  $content_paragraph_1 = _paragraphs_collection_bootstrap_demo_create_text_paragraphs('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');
  $tabs_item_1 = _paragraphs_collection_bootstrap_demo_create_paragraph_tab_item('Tabs title #1', $content_paragraph_1);
  $content_paragraph_2 = _paragraphs_collection_bootstrap_demo_create_text_paragraphs('Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?');
  $tabs_item_2 = _paragraphs_collection_bootstrap_demo_create_paragraph_tab_item('Tabs title #2', $content_paragraph_2);
  $content_paragraph_3 = _paragraphs_collection_bootstrap_demo_create_text_paragraphs('At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.');
  $tabs_item_3 = _paragraphs_collection_bootstrap_demo_create_paragraph_tab_item('Tabs title #3', $content_paragraph_3);
  $paragraphs[] = _paragraphs_collection_bootstrap_demo_create_paragraph_tab([
    $tabs_item_1,
    $tabs_item_2,
    $tabs_item_3,
  ], 'tab-default', TRUE);

  $popover_text_paragraph = _paragraphs_collection_bootstrap_demo_create_popover_text_paragraph(t('Popover text example.'));
  $paragraphs[] = $popover_text_paragraph;
  $popover_behavior_settings = [
    'placement' => 'top',
    'trigger' => ['hover'],
    'popover_content' => $popover_text_paragraph->id(),
  ];
  $paragraphs[] = _paragraphs_collection_bootstrap_demo_create_popover_text_paragraph(t('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae enim dui. Suspendisse neque velit, scelerisque eget sagittis non, ullamcorper in metus. Nulla cursus ipsum sit amet consequat accumsan. Mauris nec magna lacus. Sed vitae felis dictum, vestibulum quam vitae, tempor felis. Duis a ornare augue. Fusce fermentum purus quis metus aliquam accumsan. Morbi eleifend tincidunt rutrum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer a ex aliquam, interdum nisi in, venenatis nunc.'), $popover_behavior_settings);

  $tooltip_behavior_settings = [
    'content' => [
      'value' => 'Tooltip text example.',
      'format' => 'plain_text',
    ],
    'placement' => 'left',
    'trigger' => ['hover'],
  ];
  $paragraphs[] = _paragraphs_collection_bootstrap_demo_create_tooltip_text_paragraph($tooltip_behavior_settings);

  return _paragraphs_collection_bootstrap_demo_create_node('Paragraphs Collection Bootstrap demo example', $paragraphs);
}

/**
 * Create demo node of a type paragraphed_content_demo.
 *
 * @param string $title
 *   Node title.
 * @param array $paragraphs
 *   Array of paragraphs entites.
 *
 * @return \Drupal\Core\Entity\EntityInterface|static
 *   Saved node entity.
 */
function _paragraphs_collection_bootstrap_demo_create_node($title, array $paragraphs) {
  $node = Node::create([
    'type' => 'bootstrap_paragraphed_content',
    'title' => $title,
    'langcode' => 'en',
    'uid' => '0',
    'status' => 1,
    'field_bootstrap_paragraphs_demo' => $paragraphs,
  ]);
  $node->save();
  return $node;
}

/**
 * Helper function to create paragraph.
 *
 * @param string $type
 *   Type of paragraph.
 * @param string $field_name
 *   Name of the field.
 * @param array $items
 *   Progress bar items.
 * @param array $behavior_settings
 *   Array of behavior settings for paragraph.
 *
 * @return \Drupal\Core\Entity\EntityInterface|static
 *   Paragraphs progress entity.
 */
function _paragraphs_collection_bootstrap_demo_create_paragraph($type, $field_name, array $items, array $behavior_settings = NULL) {
  $paragraph = Paragraph::create([
    'type' => $type,
    $field_name => $items,
  ]);
  if ($behavior_settings != NULL) {
    $paragraph->setBehaviorSettings($type, $behavior_settings);
  }
  $paragraph->save();
  return $paragraph;
}

/**
 * Helper function to create paragraphs item.
 *
 * @param string $caption
 *   Image caption.
 * @param string $filename
 *   Local filename of an image.
 * @param string $url
 *   External URL of an image.
 *
 * @return \Drupal\Core\Entity\EntityInterface|static
 *   Paragraphs entity.
 */
function _paragraphs_collection_demo_create_paragraph_carousel_item($caption, $filename, $url) {
  file_put_contents("public://$filename", file_get_contents(drupal_get_path('module', 'paragraphs_collection_bootstrap_demo') . '/' . $url));
  $image = File::create([
    'uri' => "public://$filename",
  ]);
  $image->save();

  // Image paragraph type.
  $image_paragraph = Paragraph::create([
    'type' => 'image',
    'paragraphs_image' => [
      'target_id' => $image->id(),
      'alt' => $caption . ' alternative image title',
      'title' => $caption . ' image',
    ],
  ]);
  $image_paragraph->save();

  $paragraph = Paragraph::create([
    'type' => 'pcb_carousel_item',
    'field_pcb_carousel_caption' => $caption,
    'field_pcb_carousel_slide' => [$image_paragraph],
  ]);
  $paragraph->save();
  return $paragraph;
}

/**
 * Helper function to create paragraphs item.
 *
 * @param string $title
 *   Accordion item title.
 * @param \Drupal\paragraphs\ParagraphInterface $content_paragraph
 *   Accordion item content text.
 *
 * @return \Drupal\Core\Entity\EntityInterface|static
 *   Paragraph entity.
 */
function _paragraphs_collection_bootstrap_demo_create_paragraph_accordion_item($title, ParagraphInterface $content_paragraph) {
  $title_paragraph = Paragraph::create([
    'type' => 'title',
    'langcode' => 'en',
    'paragraphs_title' => $title,
  ]);
  $title_paragraph->save();

  $paragraph = Paragraph::create([
    'type' => 'pcb_accordion_item',
    'field_pcb_accordion_title' => $title_paragraph,
    'field_pcb_accordion_content' => $content_paragraph,
  ]);
  $paragraph->save();
  return $paragraph;
}

/**
 * Helper function to create progress paragraph.
 *
 * @param array $items
 *   Progress bar items.
 *
 * @return \Drupal\Core\Entity\EntityInterface|static
 *   Paragraphs progress entity.
 */
function _paragraphs_collection_bootstrap_demo_create_progress_paragraph(array $items) {
  $progress_paragraph = Paragraph::create([
    'type' => 'pcb_progress',
    'field_pcb_progress_bar' => $items,
  ]);
  $progress_paragraph->save();
  return $progress_paragraph;
}

/**
 * Helper function to create progress bar paragraph.
 *
 * @param string $style
 *   Progress Bar style.
 * @param bool $striped
 *   Striped Progress Bar.
 * @param bool $animated
 *   Animated Progress Bar.
 * @param bool $label
 *   Progress Bar label.
 * @param int $width
 *   Progress Bar width.
 * @param int $height
 *   Progress Bar height.
 *
 * @return \Drupal\Core\Entity\EntityInterface|static
 *   Paragraphs progress bar entity.
 */
function _paragraphs_collection_bootstrap_demo_create_progress_bar_paragraph($style = '', $striped = FALSE, $animated = FALSE, $label = TRUE, $width = 25, $height = 16) {
  $progress_bar_paragraph = Paragraph::create([
    'type' => 'pcb_progress_bar',
    'field_pcb_progress_bar_height' => $height,
    'field_pcb_progress_bar_width' => $width,
  ]);
  $progress_bar_paragraph->setBehaviorSettings('pcb_progress_bar', [
    'striped' => $striped,
    'animated' => $animated,
    'label' => $label,
  ]);
  $progress_bar_paragraph->setBehaviorSettings('style', ['style' => $style]);
  $progress_bar_paragraph->save();
  return $progress_bar_paragraph;
}

/**
 * Helper function to create tab paragraph.
 *
 * @param array $items
 *   Tabs content.
 * @param bool $style
 *   Tabs style.
 * @param bool $fade
 *   Fade effect.
 *
 * @return \Drupal\Core\Entity\EntityInterface|static
 *   Paragraph entity.
 */
function _paragraphs_collection_bootstrap_demo_create_paragraph_tab(array $items, $style, $fade) {
  $paragraph = Paragraph::create([
      'type' => 'pcb_tabs',
      'field_pcb_tabs_container' => $items,
    ]);
  $paragraph->setBehaviorSettings('style', ['style' => $style]);
  $paragraph->setBehaviorSettings('pcb_tabs', ['fade' => $fade]);
  $paragraph->save();
  return $paragraph;
}

/**
 * Helper function to create tab item paragraph.
 *
 * @param string $title
 *   Accordion item title.
 * @param \Drupal\paragraphs\ParagraphInterface $content_paragraph
 *   Accordion item content text.
 *
 * @return \Drupal\Core\Entity\EntityInterface|static
 *   Paragraph entity.
 */
function _paragraphs_collection_bootstrap_demo_create_paragraph_tab_item($title, ParagraphInterface $content_paragraph) {
  $paragraph = Paragraph::create([
      'type' => 'pcb_tabs_item',
      'field_pcb_tabs_title' => $title,
      'field_pcb_tabs_content' => $content_paragraph,
    ]);
  $paragraph->save();
  return $paragraph;
}

/**
 * Helper method for creating text paragraphs.
 *
 * @param string $text
 *   Text for text paragraphs.
 *
 * @return \Drupal\paragraphs\ParagraphInterface|static
 *   Text paragraph.
 */
function _paragraphs_collection_bootstrap_demo_create_text_paragraphs($text) {
  $text_paragraph = Paragraph::create([
    'type' => 'text',
    'langcode' => 'en',
    'paragraphs_text' => $text,
  ]);
  $text_paragraph->save();
  return $text_paragraph;
}

/**
 * Helper function for creating popovers on text paragraphs.
 *
 * @param string $content
 *   Text content.
 * @param array $behavior_settings
 *   An array of popover behavior settings.
 *
 * @return \Drupal\Core\Entity\EntityInterface|static
 *   Paragraphs entity.
 */
function _paragraphs_collection_bootstrap_demo_create_popover_text_paragraph($content, array $behavior_settings = NULL) {
  $paragraph = Paragraph::create([
    'type' => 'bootstrap_popover',
    'field_pcb_demo_text' => $content,
  ]);
  if ($behavior_settings != NULL) {
    $paragraph->setBehaviorSettings('pcb_popover', $behavior_settings);
  }
  $paragraph->save();
  return $paragraph;
}

/**
 * Helper function for creating popover on image paragraphs.
 *
 * @param string $title
 *   Image title.
 * @param string $filename
 *   Image filename.
 * @param string $url
 *   Image URL.
 * @param array $behavior_settings
 *   An array of popover behavior settings.
 *
 * @return \Drupal\Core\Entity\EntityInterface|static
 *   Paragraphs entity.
 */
function _paragraphs_collection_bootstrap_demo_popover_image_paragraph($title, $filename, $url, array $behavior_settings) {
  file_put_contents("public://$filename", file_get_contents(drupal_get_path('module', 'paragraphs_collection_bootstrap_demo') . '/' . $url));
  $image = File::create([
    'uri' => "public://$filename",
  ]);
  $image->save();

  $paragraph = Paragraph::create([
    'type' => 'bootstrap_popover_image',
    'field_pcb_demo_image' => [
      'target_id' => $image->id(),
      'alt' => $title,
      'title' => $title,
    ],
  ]);
  $paragraph->setBehaviorSettings('pcb_popover', $behavior_settings);
  $paragraph->save();

  return $paragraph;
}

/**
 * Helper function for creating tooltips on text paragraphs.
 *
 * @param array $behavior_settings
 *   An array of tooltip behavior settings.
 *
 * @return \Drupal\Core\Entity\EntityInterface|static
 *   Paragraphs entity.
 */
function _paragraphs_collection_bootstrap_demo_create_tooltip_text_paragraph(array $behavior_settings) {
  $paragraph = Paragraph::create([
    'type' => 'bootstrap_tooltip',
    'field_pcb_demo_text' => t('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae enim dui. Suspendisse neque velit, scelerisque eget sagittis non, ullamcorper in metus. Nulla cursus ipsum sit amet consequat accumsan. Mauris nec magna lacus. Sed vitae felis dictum, vestibulum quam vitae, tempor felis. Duis a ornare augue. Fusce fermentum purus quis metus aliquam accumsan. Morbi eleifend tincidunt rutrum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer a ex aliquam, interdum nisi in, venenatis nunc.'),
  ]);
  $paragraph->setBehaviorSettings('pcb_tooltip', $behavior_settings);
  $paragraph->save();

  return $paragraph;
}
